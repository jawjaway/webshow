import socket
import uuid
import os
import sys
import logging
import traceback
from asyncio import Future


import tornado
import tornado.ioloop
import tornado.iostream
import tornado.escape
import tornado.gen
import tornado.httpclient
import tornado.tcpserver
import tornado.netutil


log = logging.getLogger('console')


class JRpcHandlerBase():
    def __init__(self, channel):
        self.trans = {}
        self.channel = channel

    def on_channel_active(self, channel):
        log.info('channel active: %s', channel)

    def on_channel_inactive(self, channel):
        log.info('channel inactive: %s', self.channel)

    def handle_message(self, msg):
        raise NotImplementedError()


class JRpcChannel(object):
    def __init__(self, handler_class, stream=None):
        self.handler = handler_class(self)
        self.stream = stream

    def get_handler(self):
        return self.handler

    def on_active(self, stream):
        self.stream = stream
        self.handler.on_channel_active(self)

    def on_inactive(self):
        self.stream = None
        self.handler.on_channel_inactive(self)

    def write(self, data):
        if self.stream:
            self.stream.write(data)

    async def _read_message(self, stream, protocol='netstrings'):
        await self._read_netstrings(stream)

    async def _read_netstrings(self, stream):
        while True:
            try:
                head = await stream.read_until(':'.encode())
                data = head.decode()
                msg_len = int(data.rstrip(':'))
                data = await stream.read_bytes(msg_len)
                msg = tornado.escape.json_decode(data)
                log.info('JRpcChannel recv msg %s', msg)
                await self.handler.handle_message(msg)
                tail = await stream.read_bytes(1)
                if tail.decode() != ',':
                    log.warning('tail error: %s', tail)
                    stream.close()
            except tornado.iostream.StreamClosedError:
                log.warning("jrpc stream close")
                break
            except Exception as exp:
                tb = traceback.format_exc()
                log.warning("jrpc read exception: %s\n%s", exp, tb)
                stream.close()
                break


class JRpcClient():
    def __init__(self, handler_class):
        self.handler_class = handler_class
        self.channel = JRpcChannel(handler_class)
        self.address = None

    def get_channel(self):
        return self.channel

    async def start(self, addr):
        self.server_addr = addr
        log.info('start to connect %s ...', self.server_addr)
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            stream = tornado.iostream.IOStream(sock)
            await stream.connect(self.server_addr)
            self.address = sock.getsockname()
            log.info('connect ok %s', self.address)

            self.channel.on_active(stream)
            await self.channel._read_message(stream)
            self.channel.on_inactive()
        except tornado.iostream.StreamClosedError as exp:
            log.warning('Exception %s', exp)
        tornado.ioloop.IOLoop.instance().call_later(3, self.start, self.server_addr)
        log.info('closed %s -> %s!!!', self.address, self.server_addr)


class JRpcServer(tornado.tcpserver.TCPServer):
    def __init__(self, handler_class):
        self.handler_class = handler_class
        self.conns = {}
        tornado.tcpserver.TCPServer.__init__(self)

    def start_server(self, server_port, server_ip=None):
        #self.listen(server_port)
        self.sockets = tornado.netutil.bind_sockets(server_port, server_ip)
        for sk in self.sockets:
            self.address = sk.getsockname()
            sk.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            if os.name == 'posix' and sys.platform != 'darwin':
                sk.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 60)
                sk.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 60)
                sk.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 3)
        self.add_sockets(self.sockets)

    @tornado.gen.coroutine
    def handle_stream(self, stream, address):
        channel = JRpcChannel(self.handler_class, stream)
        self.conns[address] = channel
        channel.on_active(stream)
        yield from channel._read_message(stream)
        channel.on_inactive()


def test():
    log.setLevel(logging.DEBUG)
    global jclient
    jclient = JRpcClient(('192.168.1.3', 9999))

    #loop = asyncio.get_event_loop()
    #loop.run_until_complete(jclient.test())
    #loop.close()
    #@tornado.gen.coroutine
    #def tcallback():
    #    print('ttttcb')
    #    tornado.ioloop.IOLoop.instance().add_callback(jclient.test)

    #tornado.ioloop.PeriodicCallback(tcallback, 5*1000).start()
    #tornado.ioloop.IOLoop.instance().call_at(100*1000, tcallback)
    tornado.ioloop.IOLoop.instance().add_callback(jclient.start)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    #atest()
    test()
