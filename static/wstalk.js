function patch_log() {
    console.log = function() {
        line = [];
        nline = Number(window.localStorage.logs_nline || 0);
        for (i in arguments) {
            line.push(arguments[i]);
        }
        window.localStorage['logs_line_' + nline] = JSON.stringify(line);
        window.localStorage.logs_nline = nline + 1;
    };
}

function get_logs() {
    nline = Number(window.localStorage.logs_nline || 0);
    var logs = [];
    for(x=0; x<nline; x++) {
        data = window.localStorage['logs_line_' + x];
        if (data) {
            logs.push(data);
        }
    }
    return logs;
}

function clear_logs() {
    for(x in window.localStorage) {
        if (x.indexOf('logs_line')==0)
            window.localStorage.removeItem(x);
    }
    window.localStorage.removeItem('logs_nline');
}

$(document).ready(function(){
    patch_log();
    init();
});

function base_view_construct(self) {
    self.show = ko.observable(false);
    self.activate = function() {
        console.log('activate', self);
    }
}

function TCallViewModel(owner) {
    // Data
    var self = this;
    base_view_construct(self);
    self.peerid = ko.observable("");
    self.state = ko.observable("idle");
    self.video = ko.observable(true);
    self.peer_sdp = null;
    self.peer_candidates = [];
    self.cctext = ko.computed(function() {
        if (self.state() == 'idle')
            return self.video() ? 'VideoChat' : 'AudioChat';
        else if (self.state() == 'incoming')
            return 'Answer';
        else
            return 'Stop';
    });
    self.incalltext = ko.observable("call incoming");

    self.activate = function() {
        txchannel.bind_call_handler(call_msg_handle);
    };

    self.deactivate = function() {
        self.state("idle");
        rtc_stop();
    };

    var calltag;
    var rtc_control;
    var localStream;
    var localMedia;
    var remoteMedia;

    // Behaviours
    self.call = function(tabpage) {
        if (self.state() == "idle") {
            call_start();
        } else if (self.state() == "incoming") {
            self.answer();
        } else {
            call_stop();
        }
    };

    function call_start() {
        if (!self.peerid()) {
            alert('no callee');
            return;
        }
        localMedia = document.getElementById('localVideo');
        remoteMedia = document.getElementById('remoteVideo');
        if (localStream) { localStream.stop();}
        //have to open media device every call begin
        use_local_media(localMedia, self.video(), function() {
            self.state("starting");
            setup_peer_media(localStream, remoteMedia);
        });
    }

    function use_local_media(local_media, is_video, on_success, on_error) {
        var mediaConstraints = is_video ? {audio: true, video: true} : {audio: true, video: false};
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(function(stream) {
                console.log("User has granted access to local media.");
                local_media.srcObject = stream;
                local_media.style.opacity = 1;
                localStream = stream;
                on_success();
            }).catch(on_error ? on_error : function(error) {
                console.log('getUserMedia fail', error);
            });
    }

    function setup_peer_media(local_stream, remote_media) {
        var sdp_data = null;
        var candidates_ready = false;
        var sdp = null;
        var candidates = [];
        rtc_control = new RtcControl(local_stream, remote_media, null, null, self.video(), function(sdp) {
            console.log('on_sdp:', sdp);
            sdp_data = sdp;
            if (candidates_ready) {
                setup_call(sdp_data, candidates)
            }
        }, function(candidate) {
            console.log('on_candidate:', candidate);
            if (candidate==null) {
                candidates_ready = true;
                if (sdp_data)
                    self.setup_call(sdp_data, candidates)
            } else
                candidates.push(candidate);
        });
    }
    self.setup_call = function(sdp, candidates) {
        self.state("starting");
        var peerid = self.peerid();
        if (peerid.indexOf('@') < 0)
            peerid = peerid + '@' + user_profile.realm;
        self.callid = str_gen(8);

        req = {'name': 'call',
               'callid': self.callid,
               'clientid': user_profile.clientid,
               'tu': peerid,
               'fu': user_profile.uri,
               'p2p': {'sdp': sdp.sdp.replace(/UDP\/TLS\/RTP\/SAVPF/g, 'RTP/SAVPF'), 'candidates': candidates}
               };
        txchannel.send_request(req, function(resp){
            if (resp.result == 'ok') {
                self.state("ringing");
                self.incalltext("ringring");
            }
        });
    };
    self.answer = function() {
        self.state("anwsered");
        localMedia = document.getElementById('localVideo');
        remoteMedia = document.getElementById('remoteVideo');
        //if (localStream) {localStream.stop();}
        //should open media device every call
        use_local_media(localMedia, self.video(), function() {
            recv_peer_media(localStream, remoteMedia);
        });
    };

    function recv_peer_media(local_stream, remote_media) {
        function call_answer(sdp, candidates) {
            answer_req = {'name': 'answer', p2p: {sdp: sdp.sdp.replace(/UDP\/TLS\/RTP\/SAVPF/g, 'RTP/SAVPF'), 'candidates': candidates}, callid: self.callid, clientid:user_profile.clientid};
            txchannel.send_request(answer_req, function (resp) {
                console.log('answer resp')
            });
        }
        var sdp_data = null;
        var candidates_ready = false;
        var candidates = [];
        rtc_control = new RtcControl(local_stream, remote_media, {sdp: self.peer_sdp, type: 'offer'}, self.peer_candidates, self.video(), function(sdp) {
            console.log('on_sdp:', sdp);
            sdp_data = sdp;
        }, function(candidate) {
            console.log('on_candidate:', candidate);
            if (candidate == null) {
                candidates_ready = true;
                if (sdp_data)
                    call_answer(sdp_data, candidates);
            } else {
                candidates.push(candidate);
            }
        });
    }

    function call_stop() {
        console.log("call_stop");
        self.peer_candidates = [];
        self.state("idle");
        req = {name: 'bye', callid: self.callid, clientid:user_profile.clientid};
        txchannel.send_request(req, function (resp) {
            console.log('recv bye resp', resp)
        });
        try {
            rtc_stop();
        } catch (error) {
            console.log('rtc_stop exception:', error, error.toString())
        }
    }

    function rtc_stop() {
        localMedia.style.opacity = 0;
        remoteMedia.style.opacity = 0;
        if (localStream) { // shall stop media device every call end
            console.log('localMedia', localStream);
            media_stream_stop(localStream);
            localStream = null;
        }
        if (rtc_control) {
            rtc_control.stop();
            rtc_control = null;
        }
    }

    function call_msg_handle(msg) {
        //console.log('call msg event:', msg.eventid);
        if (msg.name == 'call') {
            self.peer_sdp = msg.p2p.sdp;
            self.peer_candidates = msg.p2p.candidates;
            self.state('incoming');
            self.callid = msg.callid;
            if (msg.p2p.sdp.indexOf("m=video") > 0) {
                self.video(true);
                self.incalltext("video call from " + msg['fu']);
            } else {
                self.video(false);
                self.incalltext("audio call from " + msg['fu']);
            }
            resp = {result: 'ok', reqid: msg.reqid, rname: msg.name, callid: msg.callid};
            txchannel.send_msg(resp);
        }
        else if (msg.name == 'answer') {
            console.log('recv answer with sdp:', msg.p2p.sdp, '\nwith candidates:\n', msg.p2p.candidates);
            if (msg.p2p.sdp) {
                rtc_control.set_remote_sdp({'type': 'answer', 'sdp': msg.p2p.sdp}, function () {
                    if (msg.p2p.candidates)
                        rtc_control.set_remote_candidates(msg.p2p.candidates);
                });
            }
            resp = {result: 'ok', reqid: msg.reqid, rname: msg.name, callid: msg.callid};
            txchannel.send_msg(resp);
        }
        else if (msg.name == 'bye') {
            rtc_stop();
            self.state("idle");
            resp = {result: 'ok', reqid: msg.reqid, rname: msg.name, callid:msg.callid};
            txchannel.send_msg(resp);
        }
        else
            console.log('unknown event');
    }
}


function RtcControl(local_stream, remote_media, peer_sdp, peer_candidates, is_video, on_sdp, on_candidate, on_error) {
    var self = this;

    var remoteStream;
    var pc;
    var sdpConstraints = {'mandatory': {
        'OfferToReceiveAudio': true,
        'OfferToReceiveVideo': (is_video ? true : false)
    }};

    function createPeerConnection(pcConfig) {
        try {
            console.log("createPeerConnection", pcConfig);
            var pconn = new RTCPeerConnection(pcConfig);
        } catch (e) {
            console.log('Failed to create PeerConnection, exception: ' + e.message);
            alert('Cannot create RTCPeerConnection object;\n WebRTC is not supported by this browser.');
            return null;
        }
        return pconn;
    }

    function start_peer_conn() {
        console.log('Creating PeerConnection.');
        //var pc_config = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
        var pc_config = {"iceServers": [{"url": "stun:vvtools.com:3478"}]};
        var pconn = createPeerConnection(pc_config);
        pconn.onicecandidate = function (event) {
            console.log('onicecandidate:', event.candidate);
            on_candidate(event.candidate);
        };
        pconn.onaddstream = function(event) {
            console.log('Remote stream added.');
            remote_media.srcObject = event.stream;
            remoteStream = event.stream;
            remote_media.style.opacity = 1;
        };
        pconn.onremovestream = function(event) {console.log('Remote stream removed.');};
        pconn.onsignalingstatechange = function(event) {console.log('onsignalingstatechange', event);};
        pconn.oniceconnectionstatechange = function(event) {console.log('oniceconnectionstatechange', event);};

        console.log('Adding local stream.');
        pconn.addStream(local_stream);
        return pconn;
    }

    function create_sdp_offer(constraints) {
        pc.createOffer(function(sessionDescription) {
                pc.setLocalDescription(sessionDescription,
                                    function(){"onSetSessionDescriptionSuccess"},
                                    function(){"onSetSessionDescriptionError"});
                console.log('local sdp', sessionDescription);
                on_sdp(sessionDescription);
            }, function(error){
                console.log("onCreateSessionDescriptionError", error);}
            , constraints);
    }

    function create_sdp_answer(constraints) {
        pc.createAnswer(function(sessionDescription) {
                pc.setLocalDescription(sessionDescription,
                                    function(){console.log("onSetSessionDescriptionSuccess")},
                                    function(error){console.log("onSetSessionDescriptionError", error)});
                console.log('local sdp', sessionDescription);
                on_sdp(sessionDescription);
            }, function(error){
                console.log("onCreateSessionDescriptionError", error);}
            , constraints);
    }

    self.set_remote_sdp = function(remote_sdp, on_ok, on_error) {
        pc.setRemoteDescription(new RTCSessionDescription(remote_sdp),
            on_ok ? on_ok : function(){console.log("onSetSessionDescriptionSuccess");},
            on_error ? on_error : function(error){console.log("onSetSessionDescriptionError:", error, error.toString());}
        );
    };

    self.set_remote_candidates = function (peer_candidates) {
        for (i in peer_candidates) {
            peer_candidate = peer_candidates[i];
            var candidate = new RTCIceCandidate(peer_candidate);
            pc.addIceCandidate(candidate);
            console.log('add condidate:', candidate);
        }
    };

    self.stop = function() {
        if (remoteStream) {
            media_stream_stop(remoteStream);
            remoteStream = null;
        }
        if (pc) {
            pc.close();
            pc = null;
        }
    };
    
    self.handle_peer_candidate = function(peer_candidate) {
        console.log('peer candidate:', peer_candidate);
        var candidate = new RTCIceCandidate(peer_candidate);
        //console.log(pc, candidate);
        pc.addIceCandidate(candidate);
    };

    pc = start_peer_conn();
    console.log('**** peer conn ****:', pc);
    if (peer_sdp) {
        console.log('peer sdp', peer_sdp);
        self.set_remote_sdp(peer_sdp, function() {
            console.log('peer candidate in sdp ... ...');
            for (i in peer_candidates) {
                self.handle_peer_candidate(peer_candidates[i]);
            }
            peer_candidates = [];
            create_sdp_answer(sdpConstraints);
        });
    }
    else {
        create_sdp_offer(sdpConstraints);
    }
}

user_profile = {
    'userid': '',
    'realm': '',
};

function TLoginViewModel(owner) {
    // Data
    var self = this;
    base_view_construct(self);

    self.uid = ko.observable("");
    self.pswd = ko.observable("");
    self.online = ko.observable(false);
    var nonce = "";

    self.activate = function() {
        owner.hide_all_views(self);
    }
    // Behaviours
    self.login = function(tabpage) {
        txchannel.start( function() {
            console.log("channel open");
            var instance_uuid = getCookie('instance_uuid');
            register(self.uid(), get_realm(), self.pswd(), instance_uuid, nonce, function(resp) {
                console.log('register resp', resp);
                if (resp.result == 'ok') {
                    self.pswd('');
                    nonce = resp.nonce;
                    user_profile.userid = self.uid();
                    user_profile.realm = get_realm();
                    user_profile.clientid = resp.clientid;
                    user_profile.uri = resp.uri;
                    self.show(false);
                    owner.goview("logoutview");
                    owner.goview("callview");
                } else {
                    alert('register fail:' + resp.result);
                }
            });
        }, function(event) {
            console.log('channel closed');
            owner.deactivate_views();
            owner.goview("loginview");
        });
    };
}

function TLogoutViewModel(owner) {
    var self = this;
    base_view_construct(self);
    self.uid = ko.observable();
    
    self.activate = function() {
        self.uid(user_profile.userid);
    };

    self.logout = function(tabpage) {
        if (txchannel) {
            txchannel.close();
        }
        owner.goview("loginview");
    };
};

function TRootViewModel(owner) {
    // Data
    var self = this;
    //self.show = ko.observable(false);
    self.views = {};
    self.views.loginview = new TLoginViewModel(self);
    self.views.logoutview = new TLogoutViewModel(self);
    self.views.callview = new TCallViewModel(self);
    self.views.logs = new TLogsViewModel(self);

    //ko.applyBindings(self.views.loginview, $("#loginview")[0]);
    //ko.applyBindings(self.views.loginview, $("#loginview")[0]);

    self.goview = function(name) {
        self.views[name].show(true);
        self.views[name].activate();
    };
    self.hide_all_views = function(exceptme) {
        for (i in self.views)
            self.views[i].show(self.views[i] == exceptme);
    };
    self.deactivate_views = function() {
        for (i in self.views)
            if (self.views[i].deactive)
                self.views[i].deactive();
    };
    self.goview("loginview");
};

function init()
{
    var rootview = new TRootViewModel(null);
    ko.applyBindings(rootview, $("#rootview")[0]);
}

function TransChannel() {
    var self = this;
    var txid = 0;
    var trans = {};
    function get_txid() {
        return ++txid;
    }
    self.call_handler = function(msg) {
        console.log('no handler for msg:', msg);
    };
    self.bind_call_handler = function(handler) {
        self.call_handler = handler;
    };

    var wssock;
    self.start = function(onopen, onclose) {
        wssock = new JSocket(get_ws_uri());
        wssock.onopen = onopen;
        wssock.onclose = onclose;
        wssock.onmessage = self.onmessage;
        self.send_msg = wssock.sendmessage;
    };

    self.send_msg = function() {
        console.log('error: socket not start');
    };

    self.send_request = function(req, cb) {
        req.reqid = '' + get_txid();
        wssock.sendmessage(req);
        trans[req.reqid] = cb;
    };
    self.onmessage = function(event) {
        //console.log('recv:', event.data);
        var msg = JSON.parse(event.data);
        console.log('recv msg:', msg);
        if (msg.name) {
            self.call_handler(msg);
        }
        else {
            var cb = trans[msg.reqid];
            if (cb) {
                cb(msg);
                delete trans[msg.reqid];
            } else
                console.log("no callback");
        }
    };
    self.close = function() {
        wssock.close();
        wssock = null;
    }
}

var txchannel;
txchannel = new TransChannel();

//----login
function register(uid, realm, password, uuid, nonce, result_callback) {
    console.log('register: %s %s', uid, realm);
    function make_req(auth_nonce) {
        var req = {name: 'auth', uri: uid + '@' + realm}
        if (auth_nonce) {
            var cnonce = '' + Math.round(Math.random()*100000000);
            var ruri = get_ws_uri();
            /*var authstr = hex_md5(hex_md5(name + ':' + realm + ':' + password) + ':' +
                                 auth_nonce +':'+ 
                                 hex_md5('REGISTER' + ':' + ruri));*/
            var authstr = hex_md5(hex_md5(uid + ':' + realm + ':' + password) +':'+ auth_nonce +':'+ cnonce);
            req.nonce = auth_nonce;
            req.cnonce = cnonce;
            req.qop = 'auth-zzz';
            req.auth_rsp = authstr;
        }
        return req;
    }
    txchannel.send_request(make_req(nonce), function(resp){
        console.log('reg response result:', resp.result);
        if (resp.result == 'auth-required' || resp.result == 'auth-fail') {
            txchannel.send_request(make_req(resp.nonce), function(resp){
                result_callback(resp);
            });
        }
        else
            result_callback(resp);
    });
}

function media_stream_stop(media_stream) {
    var atracks = media_stream.getAudioTracks();
    for (i in atracks) {
        atracks[i].stop();
    }
    var vtracks = media_stream.getVideoTracks();
    for (i in vtracks) {
        vtracks[i].stop();
    }
}


function JSocket(server_url)
{
    var self = this;
    var socket;
    if ("WebSocket" in window) {
        socket = new WebSocket(server_url); //"ws://192.168.0.100:8888/schannel");
    } else {
        socket = new MozWebSocket(server_url);
    }

    socket.binaryType = "arraybuffer";  //zzz

    self.onopen = function(event) { console.log('socket onopen')};
    self.onmessage = function(event) { console.log('socket onmessage', event)};
    self.onclose = function(event) {console.log('socket onclose')};
    self.onerror = function(event) {console.log('socket onerror', event)};

    socket.onopen = function(event) {self.onopen(event)};
    socket.onclose = function(event) {self.onclose(event)};
    socket.onerror = function(event) {self.onerror(event)};
    socket.onmessage = function(event) {self.onmessage(event)};

    self.sendmessage = function(msg) {
        var data = JSON.stringify(msg)
        socket.send(data);
        console.log('socket send message:', msg);
    }

    self.send = function(data) {
        socket.send(data);
        console.log('socket send:', data);
    }

    self.close = function() {
        console.log('close');
        socket.close();
    }
}

function str_gen(slen)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < slen; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getCookie(name) {
  var parts = document.cookie.split(name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function TLogsViewModel(owner) {
    var self = this;
    base_view_construct(self);
    self.logs = ko.observable([]);
    self.show_log = function() {
        data = get_logs();
        self.logs(data);
    };
    self.clear_log = function() {
        clear_logs();
        data = get_logs();
        self.logs(data);
    }
}
