#!/usr/bin/env python
import os.path
import uuid
import logging
import time
import hashlib
import threading

import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket
import tornado.httpserver
import tornado.concurrent
import tornado.escape
from tornado.options import define, options, parse_command_line

import utils
import jrpc

log = logging.getLogger("tornado.general")


define("port", default=8732, help="run on the given port", type=int)
define("sslport", default=8733, help="run on the given ssl port", type=int)
define("wsport", default=8734, help="ws port", type=int)
define("wssport", default=8735, help="wss port", type=int)
define("jsvr_ip", default="127.0.0.1", help="jrpc_server ip", type=str)
define("jsvr_port", default=9999, help="jrpc_server port", type=int)
define("rpc_inbound_port", default=9888, help="jrpc inbound port", type=int)
define("my_ip", default=utils.getNetworkIp(), help="my_ip", type=str)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", HomeHandler),
            (r"/test", TestHandler)
        ]

        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            autoescape=None,
            cookie_secret="zJXytzETSN+FqooyLKNJTvkT8xjfzEVwkuQUfEXekQz=",
            debug=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class HomeHandler(tornado.web.RequestHandler):
    def get(self):
        instance_uuid = self.get_cookie('instance_uuid')
        if not instance_uuid:
            log.info('no cookie, set a cookie')
            self.set_cookie('instance_uuid', uuid.uuid4().hex, expires_days=30)
        if self.request.protocol == 'http':
            ws_uri = 'ws://' + str(options.my_ip) + ':' + str(options.wsport) + '/wstalk'
        else:
            ws_uri = 'wss://' + str(options.my_ip) + ':' + str(options.wssport) + '/wstalk'
        self.render("wstalk.html", ws_uri=ws_uri, realm="test.com")


class TestHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self):
        yield from self.handle_message({})

    async def test(self):
        req = tornado.httpclient.HTTPRequest(method='GET', url="http://baidu.com")
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(req)
        print('----------', response)
        return response


    async def handle_message(self, msg):
        log.info('register test')
        resp = await self.test()
        print('rrrrrrrrrrrrrrrrrrr', resp)
        self.write('ok')


class WsApplication(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/wstalk", WsAppConnection),
        ]

        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            autoescape=None,
            cookie_secret="zJXytzETSN+FqooyLKNJTvkT8xjfzEVwkuQUfEXekQz=",
            debug=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class WsAppConnection(tornado.websocket.WebSocketHandler):
    contacts = {}

    def check_origin(self, origin):
        return True

    def open(self):
        log.info("ws opened from %s", self.request.remote_ip)
        self.recv = '%s' % self.request.remote_ip
        self.uri = ''
        self.registered = False
        self.contact_addr = ''
        self.uuid = ''
        self.clientid = ''
        self.callleg = ''
        self.auth = ''
        self.trans = {}
        self.ua = UserAgent(self)

    @tornado.gen.coroutine
    def on_close(self):
        log.info("ws closed")
        yield from self.ua.unregister()

    @tornado.gen.coroutine
    def on_message(self, message):
        log.debug("on_message")
        msg = tornado.escape.json_decode(message)
        yield from self.handle_message(msg)
        #tornado.ioloop.IOLoop.current().spawn_callback(self.handle_message, msg)

    async def handle_message(self, msg):
        name = msg.get('name')
        reqid = msg.get('reqid')
        if name:
            resp = await self.ua.handle_client_request(name, msg)
            resp['reqid'] = reqid
            resp['rname'] = name
            self.send_message(resp)
        else:
            xx = self.trans.get(reqid)
            if xx:
                xx.set_result(msg)
            else:
                print('not reqid', reqid)

    def send_message(self, msg):
        log.debug('WsAppConnection send_message: %s', msg)
        self.write_message(msg)

    async def do_request(self, req):
        log.info('WsAppConnection do_request: %s', req)
        reqid = str(uuid.uuid4())
        req['reqid'] = reqid
        future = tornado.concurrent.Future()
        self.trans[reqid] = future
        self.send_message(req)
        timeout_h = tornado.ioloop.IOLoop.instance().call_later(60, self.timeout_handle, future)
        await future
        tornado.ioloop.IOLoop.instance().remove_timeout(timeout_h)
        return future.result()

    def timeout_handle(self, future):
        if future.done():
            return
        else:
            future.set_result({'result': 'timeout'})

def sdp_candidates_split(sdp):
    sdp_lines = sdp.split('\r\n')  # diff with splitlines while end empty line
    new_sdp_lines = []
    candidates = []
    mline = ''
    sdpMLineIndex = -1
    for line in sdp_lines:
        if line.startswith('m='):
            words = line.split()
            mline = words[0][len('m='):]
            sdpMLineIndex += 1
            new_sdp_lines.append(line)
        elif line.startswith('a=candidate'):
            candidate = {'sdpMid': mline, 'sdpMLineIndex': sdpMLineIndex, 'candidate': line.lstrip('a=').rstrip('\r\n')}
            candidates.append(candidate)
        else:
            new_sdp_lines.append(line)
    return '\r\n'.join(new_sdp_lines), candidates


def sdp_candidates_combine(sdp, candidates):
    log.info('sdp_candidates_combine: sdp with %d candidates', len(candidates))
    sdp_lines = sdp.split('\r\n')  # diff with splitlines while end empty line
    new_sdp_lines = []
    mline = ''
    aline = ''
    for line in sdp_lines:
        if mline:
            if line.startswith('a='):
                aline = 'be'
                new_sdp_lines.append(line)
            elif aline == 'be':  # now aline end
                aline = ''
                for candidate in candidates:
                    if candidate.get('sdpMid') == mline:
                        new_line = candidate['candidate'].rstrip('\r\n')
                        if not new_line.startswith('a='):
                            new_line = 'a=' + new_line
                        new_sdp_lines.append(new_line)
                new_sdp_lines.append(line)
                if line.startswith('m='):
                    words = line.split()
                    mline = words[0][len('m='):]
                else:
                    mline = ''
            else:  # not aline meet
                new_sdp_lines.append(line)
        else:
            new_sdp_lines.append(line)
            if line.startswith('m='):
                words = line.split()
                mline = words[0][len('m='):]
    return '\r\n'.join(new_sdp_lines)


def sdp_check_fix(sdp):
    sdp_lines = sdp.split('\r\n')  # diff with splitlines while end empty line
    new_sdp_lines = []
    pts = []
    for line in sdp_lines:
        if line.startswith('m='):
            words = line.split()
            pts = words[3:]
            new_sdp_lines.append(line)
        elif line.startswith('a=fmtp:'):
            words = line.split()
            pt = words[0][len('a=fmtp:'):]
            bug_line = False
            for word in words:
                if word.startswith('apt='):
                    apt = word[len('apt='):]
                    if apt not in pts:
                        bug_line = True
                        break
            if bug_line:
                if new_sdp_lines[-1].startswith('a=rtpmap:' + pt):
                    new_sdp_lines.pop()
            else:
                new_sdp_lines.append(line)
        else:
            new_sdp_lines.append(line)
    return '\r\n'.join(new_sdp_lines)


def make_candidate_info(candidate):
    info_body = 'a=mid:%s\r\na=m-line-id:%s\r\n%s' % (candidate['sdpMid'], candidate['sdpMLineIndex'], candidate['candidate'])
    info_line = [('Info-Package', 'trickle-ice'), ('Content-Disposition', 'Info-Package')]
    return info_line, info_body


class JRpcHandler(jrpc.JRpcHandlerBase):
    def on_channel_active(self, channel):
        self.trans = {}
        log.info('channel active: %s', channel)

    def on_channel_inactive(self, channel):
        log.info('channel inactive: %s', self.channel)

    async def handle_message(self, msg):
        log.debug('handle_message: %s', msg)
        name = msg.get('name')
        reqid = msg.get('reqid')
        if name:
            clientid = msg.get('clientid')
            ua = gw_app.find_ua(clientid)
            if ua:
                resp = await ua.handle_as_request(name, msg)
                resp['reqid'] = reqid
                resp['rname'] = name
            else:
                resp = {'result': 'not-client', 'reqid': reqid, 'rname': name}
                log.warning('not client: %s', clientid)
            self.send_msg(resp)
        else:
            xx = self.trans.get(reqid)
            if xx:
                if not xx.done() and not xx.cancelled():
                    xx.set_result(msg)
                else:
                    log.debug('req %s timeout discard', reqid)
            else:
                print('not reqid', reqid)

    async def do_request(self, req):
        log.info('JRpcHandler do_request: %s', req)
        reqid = str(uuid.uuid4())
        req['reqid'] = reqid
        future = tornado.concurrent.Future()
        self.trans[reqid] = future
        self.send_msg(req)
        timeout_h = tornado.ioloop.IOLoop.instance().call_later(60, self.timeout_handle, future)
        await future
        tornado.ioloop.IOLoop.instance().remove_timeout(timeout_h)
        return future.result()

    def timeout_handle(self, future):
        if future.done():
            return
        else:
            future.set_result({'result': 'timeout'})

    def send_msg(self, rmsg):
        log.info('JRpcHandler send msg: %s', rmsg)
        jmsg = tornado.escape.json_encode(rmsg)
        nmsg = "%d:%s," % (len(jmsg), jmsg)
        self.channel.write(nmsg.encode(encoding="utf-8"))


class UserAgent(object):
    def __init__(self, ua_channel):
        self.clientid = ''
        self.registered = False
        self.ua_channel = ua_channel

    async def handle_client_request(self, name, req):
        if name == 'auth':
            resp = await self.handle_auth(req)
        elif name == 'call':
            resp = await self.handle_outbound_call(req)
        elif name == 'answer':
            resp = await self.handle_client_answer(req)
        elif name == 'bye':
            resp = await self.handle_client_bye(req)
        else:
            resp = {'result': 'no-handler'}
        return resp

    async def handle_as_request(self, name, req):
        if name == 'call':
            resp = await self.handle_inbound_call(req)
        elif name == 'answer':
            resp = await self.handle_as_answer(req)
        elif name == 'bye':
            resp = await self.handle_as_bye(req)
        else:
            resp = {'result': 'no-handler'}
        return resp

    async def handle_inbound_call(self, req):
        log.debug('handle_inbound_call')
        body = req.get('p2p', {}).get('body')
        sdp = req.get('p2p', {}).get('sdp')
        candidates = req.get('p2p', {}).get('candidates')
        if body and (not sdp or not candidates):
            sdp, candidates = sdp_candidates_split(body)
            req['p2p']['sdp'] = sdp
            req['p2p']['candidates'] = candidates
            log.debug('new sdp\n%s\nwith canditates:\n%s', sdp, candidates)
        resp = await self.ua_channel.do_request(req)
        return resp

    async def handle_as_answer(self, req):
        log.debug('handle_as_answer')
        body = req.get('p2p', {}).get('body')
        sdp = req.get('p2p', {}).get('sdp')
        candidates = req.get('p2p', {}).get('candidates')
        if body and (not sdp or not candidates):
            sdp, candidates = sdp_candidates_split(body)
            req['p2p']['sdp'] = sdp
            req['p2p']['candidates'] = candidates
            log.debug('new sdp\n%s\nwith canditates:\n%s', sdp, candidates)
        resp = await self.ua_channel.do_request(req)
        return resp

    async def handle_as_bye(self, req):
        log.debug('handle_as_bye')
        resp = await self.ua_channel.do_request(req)
        return resp

    async def handle_outbound_call(self, req):
        log.debug('handle_outbound_call')
        req['gateway'] = '%s:%d' % gw_app.get_inbound_address()
        candidates = req.get('p2p', {}).get('candidates')
        sdp = req.get('p2p', {}).get('sdp')
        if sdp and candidates:
            new_sdp = sdp_check_fix(sdp)
            body = sdp_candidates_combine(new_sdp, candidates)
            req['p2p']['sdp'] = new_sdp
            req['p2p']['body'] = body
        resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(req)
        return resp

    async def handle_client_answer(self, req):
        log.debug('handle_client_answer')
        req['gateway'] = '%s:%d' % gw_app.get_inbound_address()
        candidates = req.get('p2p', {}).get('candidates')
        sdp = req.get('p2p', {}).get('sdp')
        if sdp and candidates:
            new_sdp = sdp_check_fix(sdp)
            body = sdp_candidates_combine(new_sdp, candidates)
            req['p2p']['sdp'] = new_sdp
            req['p2p']['body'] = body
        resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(req)
        return resp

    async def handle_client_bye(self, req):
        log.debug('handle_client_bye')
        req['gateway'] = '%s:%d' % gw_app.get_inbound_address()
        resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(req)
        return resp

    async def handle_auth(self, req):
        log.debug('handle_auth')
        auth_rsp = req.get('auth_rsp')
        self.uri = req.get('uri')
        self.uuid = uuid.uuid4().hex
        if not auth_rsp:
            resp = {'nonce': utils.make_nonce(self.uri), 'result':'auth-required'}
        else:
            resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(req)
            if resp.get('result') == 'ok':
                self.clientid = resp.get('clientid')
                gw_app.bind_ua(self.clientid, self)
                register_req = {'name': 'register', 'uri': self.uri, 'clientid': self.clientid,
                                'gateway': '%s:%d' % gw_app.get_inbound_address(), 'loc': 'update'}
                reg_resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(register_req)
                if reg_resp.get('result') == 'ok':
                    self.registered = True
                else:
                    resp['result'] = 'register-fail-' + reg_resp.get('result')
        return resp

    async def unregister(self):
        if self.registered:
            register_req = {'name': 'register', 'loc': 'clear', 'uri': self.uri, 'clientid': self.clientid,
                            'gateway': '%s:%d' % gw_app.get_inbound_address()}
            reg_resp = await gw_app.outbount_agent.get_channel().get_handler().do_request(register_req)
            log.info('unregister %s', reg_resp)
            self.registered = False


class GatewayApp(object):
    def __init__(self):
        self.uamap = {}
        self.outbount_agent = jrpc.JRpcClient(JRpcHandler)
        self.inbount_agent = jrpc.JRpcServer(JRpcHandler)
        self.wsapp = WsApplication()
        self.wsapp = WsApplication()
        self.app = Application()

    def start(self):
        tornado.ioloop.IOLoop.instance().add_callback(self.outbount_agent.start, addr=(options.jsvr_ip, options.jsvr_port))
        tornado.ioloop.IOLoop.instance().add_callback(self.inbount_agent.start_server, server_port=options.rpc_inbound_port, server_ip=options.my_ip)
        #self.wsapp.listen(options.wsport)
        http_server = tornado.httpserver.HTTPServer(self.app)
        http_server.listen(options.port)

        http_server_ws = tornado.httpserver.HTTPServer(self.wsapp)
        http_server_ws.listen(options.wsport)
        #http_server2 = tornado.httpserver.HTTPServer(self.wsapp)
        #http_server2.listen(options.wsport)

        https_server = tornado.httpserver.HTTPServer(self.app, ssl_options= {
                "certfile": os.path.join(".", "vvtools.com.int.crt"),
                "keyfile": os.path.join(".", "vvtools.com.key"), })
        https_server.listen(options.sslport)

        https_server2 = tornado.httpserver.HTTPServer(self.wsapp, ssl_options= {
                "certfile": os.path.join(".", "vvtools.com.int.crt"),
                "keyfile": os.path.join(".", "vvtools.com.key"), })
        https_server2.listen(options.wssport)

    def bind_ua(self, clientid, ua):
        self.uamap[clientid] = ua

    def find_ua(self, clientid):
        return self.uamap.get(clientid)

    def get_inbound_address(self):
        return self.inbount_agent.address

    def get_outbound_address(self):
        return self.outbount_agent.address


gw_app = GatewayApp()


def main():
    log.setLevel(logging.DEBUG)
    parse_command_line()

    gw_app.start()
    log.info("start on %s:%d/%d", options.my_ip, options.port, options.sslport)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
