#!/usr/bin/env python
import traceback
import socket
import functools
import time
import struct
import binascii
import base64
import random
import xml.etree.ElementTree
import json
import os
import string
import logging


def parse_elementtree(element):
    node = dict()
    for child in element:  # element's children
        dk, dv = parse_elementtree(child)
        node.setdefault(dk, []).append(dv)

    # convert all single-element lists into non-lists
    for key, value in list(node.items()):
        if len(value) == 1:
            node[key] = value[0]

    if len(list(element.items())):
        node['_attrs'] = {}
        node['_attrs'].update(list(element.items()))

    if element.text and not set(element.text) <= set(' \r\n\t'):  # subset check can not change to >
        if len(node):
            node['_text'] = element.text
        else:
            node = element.text
    #discard text in element.tail
    return element.tag, node


def xml_to_dict(xmlstr):
    elm = xml.etree.ElementTree.fromstring(xmlstr)
    tt = parse_elementtree(elm)
    return {tt[0]: tt[1]}


def str_range(start, stop, closed=False):
    if len(start) != len(stop):
        return []
    for i in range(0, len(start)):
        if start[i] != stop[i]:
            break
    base = start[0:i]
    nlen = len(start) - i
    nstart, nstop = int(start[i:]), int(stop[i:]) + 1 if closed else 0
    xformat = '%%s%%0%dd' % nlen
    return [xformat % (base, x) for x in range(nstart, nstop)]


def log_exception(view_func):
    """ log exception decorator for a view,
    """
    log = logging.getLogger('console')

    def _decorator(*args, **kwargs):
        try:
            response = view_func(*args, **kwargs)
        except:
            tb = traceback.format_exc()
            log.warning('Exception: %s', tb)
            raise
        return response
    return functools.wraps(view_func)(_decorator)


def make_nonce(uid):
    now = int(time.time())
    xcrc = binascii.crc32(uid.encode(encoding="utf-8")) ^ now
    exi = random.randint(0, 0xffffffff)

    xbuf = struct.pack('>I', now) + struct.pack('>I', xcrc) + struct.pack('>I', exi)
    print(hex(now), hex(xcrc))
    return base64.b64encode(xbuf).decode(encoding="utf-8")


def check_nonce(nonce, uid):
    xbuf = base64.b64decode(nonce)
    if len(xbuf) < 8:
        return 'wrong-nonce-length'
    ntime = struct.unpack('>I', xbuf[0:4])[0]
    tl = int(time.time()) - ntime
    if tl < 0 or tl > 3600 * 24 * 365 * 3:
        return 'wrong-time'
    if tl > 3600 * 24 * 3:
        return 'stale-nonce'
    xcrc = struct.unpack('>i', xbuf[4:8])[0] ^ ntime
    icrc = binascii.crc32(uid)
    if icrc != xcrc:
        return 'wrong-uid'
    return 'ok'


def str_gen(size=6, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def hex_str_gen(size=8):
    return binascii.b2a_hex(os.urandom(size))

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('1.0.0.0', 99))
    return s.getsockname()[0]


def jrpc(addr, data):
    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sk.connect(addr)
    req = json.dumps(data)
    print('send:', req)
    sk.send('%d:%s,' % (len(req), req))
    sk.settimeout(10)
    data = sk.recv(4096)
    resp = ''.join(data.split(':', 1)[1:])
    print('recv', resp)
    sk.close()
    return resp



def test():
    nonce = make_nonce('+8612345678901')
    print(nonce, check_nonce(nonce, '+8612345678901'))

if __name__ == '__main__':
    test()
